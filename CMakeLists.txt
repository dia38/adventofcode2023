cmake_minimum_required(VERSION 3.5.0)
project(AventOfCode2023 VERSION 0.1.0 LANGUAGES C)


set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)

add_subdirectory(Day01)
add_subdirectory(Day02)
add_subdirectory(Day03)
add_subdirectory(Day04)
# add_subdirectory(Day05)
add_subdirectory(Day06)
# add_subdirectory(Day07)
# add_subdirectory(Day08)
add_subdirectory(Day09)
# add_subdirectory(Day10)
