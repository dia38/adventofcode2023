#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>

const char *number[10] =
{
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
};

char getDigit(char *parValue)
{
    char wRes = '\0';

    if (isdigit(*parValue))
    {
        wRes = *parValue;
    }
    else
    {
        for (size_t i = 0; (i < 10) && (wRes == 0); i++)
        {
            int tmp = strncmp(number[i], parValue, strlen(number[i]));
            if (tmp == 0)
            {
                wRes = '0' + i;
            }
        }
    }
    
    return wRes;
}

uint32_t doCompute(char *parLine)
{
    char wValue[3] = "";
    bool wInit = false;
    uint32_t wRes = 0;

    while (*parLine != '\0')
    {
        char wTmp = getDigit(parLine);

        if (wTmp != 0)
        {
            if (wInit)
            {
                wValue[1] = wTmp;
            }
            else
            {
                wValue[0] = wTmp;
                wValue[1] = wTmp;
                wInit = true;
            }
        }

        parLine++;
    }
    
    sscanf(wValue, "%" SCNu32, &wRes);

    return wRes;
}

int doProcessFile(FILE *input)
{
    uint32_t wRes = 0;

    uint32_t wIdx = 0;
    char line[64];

    while (!feof(input))
    {
        char * r = fgets(line, sizeof(line), input);

        if ((line[0] != '\n') && !feof(input))
        {
            uint32_t wRc = doCompute(line);

            wRes += wRc;
            fprintf(stdout, "Line %" PRIu32 " : %" PRIu32 ", total=%" PRIu32 "\n", wIdx, wRc, wRes);
        }
    }

    return wRes;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fprintf(stdout, "Calibrate Value : %" PRIu32 "\n", total);

        fclose(input);

    }

    return wres;
}
