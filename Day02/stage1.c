#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

typedef struct 
{
    uint8_t blue;
    uint8_t red;
    uint8_t green;
} ts_item;

const static ts_item mDispo ={
    .blue   = 14,
    .green  = 13,
    .red    = 12,
};

bool doCompute(ts_item *parLine)
{
    bool wRes = true;

    if (parLine != NULL)
    {
        wRes &= (parLine->blue  <= mDispo.blue );
        wRes &= (parLine->green <= mDispo.green);
        wRes &= (parLine->red   <= mDispo.red);
    }
    else
    {
        wRes = false;
    }

    return wRes;
}

int doProcessFile(FILE *input)
{
    uint32_t wRes = 0;

    uint32_t wIdx = 0;
    char line[1024];

    char * r = fgets(line, sizeof(line), input);

    while (!feof(input))
    {
        bool wOk = true;
        char *cur;
        char * token = strtok_r(line, ":", &cur);
        uint32_t wGame;

        assert(r != NULL);

        assert(token != NULL);

        int nb = sscanf(token, "Game %" SCNu32, &wGame);
        assert(nb == 1);

        // Parser SUB set
        token = strtok_r(NULL, ";", &cur);
        while ((token != NULL) && (wOk == true))
        {
            ts_item wSet = {
                .blue   = 0,
                .green  = 0,
                .red    = 0,
            };
            char *wCurSet;

            token = strtok_r(token, ",", &wCurSet);
            while ((token != NULL) && (wOk == true))
            {
                char wColor[64];
                uint8_t wNb;

                nb = sscanf(token, "%" SCNu8 " %s", &wNb, &wColor);
                assert( nb == 2);
                if (strcmp("green", wColor) == 0)
                {
                    wSet.green = wNb;
                }
                else if (strcmp("blue", wColor) == 0)
                {
                    wSet.blue = wNb;
                }
                else if (strcmp("red", wColor) == 0)
                {
                    wSet.red = wNb;
                }

                token = strtok_r(NULL, ",", &wCurSet);
            }
            wOk = doCompute(&wSet);

            fprintf(stdout, "%" PRIu32 " b:%" PRIu8 " r:%" PRIu8 " g:%" PRIu8 "\n", wGame, wSet.blue, wSet.red, wSet.green);

            token = strtok_r(NULL, ";", &cur);
        }

        if (wOk)
        {
            wRes += wGame;
        }

        r = fgets(line, sizeof(line), input);
    }

    return wRes;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fprintf(stdout, "Calibrate Value : %" PRIu32 "\n", total);

        fclose(input);

    }

    return wres;
}
