#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>

#define GRID_SIZE 140

char mGrid[GRID_SIZE][GRID_SIZE];

size_t mSize = 0;

typedef struct {
    size_t lig;

    uint32_t num1;
} ts_engine;

static ts_engine mCur[GRID_SIZE] = {
    {
        .lig = 0,

        .num1 = 0
    }
};

uint32_t doCheck(size_t parL, size_t parStart, size_t parEnd)
{
    uint32_t wRes = 0;
    bool wRc = false;

    size_t wFoundC = 0;
    size_t wFoundL = 0;

    if (parStart > 0) 
    {
        if ((parL > 0) && !wRc)
        {
            wRc = (mGrid[parL - 1][parStart - 1] == '*');

            if (wRc)
            {
                wFoundC = parStart - 1;
                wFoundL = parL - 1;
            }
        }

        if (!wRc)
        {
            wRc = (mGrid[parL][parStart - 1] == '*');
            if (wRc)
            {
                wFoundC = parStart - 1;
                wFoundL = parL;
            }
        }

        if ((parL < (mSize - 1)) & !wRc)
        {
            wRc = (mGrid[parL + 1][parStart - 1] == '*');
            if (wRc)
            {
                wFoundC = parStart - 1;
                wFoundL = parL + 1;
            }
        }
    }

    if ((parEnd < (mSize - 1)) && !wRc)
    {
        if ((parL > 0) && !wRc)
        {
            wRc = (mGrid[parL - 1][parEnd + 1] == '*');
            if (wRc)
            {
                wFoundC = parEnd + 1;
                wFoundL = parL - 1;
            }
        }

        if (!wRc)
        {
            wRc = (mGrid[parL][parEnd + 1] == '*');
            if (wRc)
            {
                wFoundC = parEnd + 1;
                wFoundL = parL;
            }
        }

        if ((parL < (mSize - 1))  && !wRc)
        {
            wRc = (mGrid[parL + 1][parEnd + 1] == '*');
            if (wRc)
            {
                wFoundC = parEnd + 1;
                wFoundL = parL + 1;
            }
        }
    }

    if ((parL > 0)  && !wRc)
    {
        for (size_t c = parStart; (c <= parEnd)  && !wRc ; c++)
        {
            wRc = (mGrid[parL - 1][c] == '*');
            if (wRc)
            {
                wFoundC = c;
                wFoundL = parL - 1;
            }
        }
    }

    if ((parL < (mSize - 1))  && !wRc)
    {
        for (size_t c = parStart; (c <= parEnd)  && !wRc; c++)
        {
            wRc = (mGrid[parL + 1][c] == '*');
            if (wRc)
            {
                wFoundC = c;
                wFoundL = parL + 1;
            }
        }
    }

    char wNum[64] = {0};

    memcpy(wNum, mGrid[parL] + parStart, parEnd -parStart + 1);

    fprintf(stdout, "%3" PRIu32 ", %3" PRIu32 "-%3" PRIu32 "  ", parL, parStart, parEnd);
    int s = sscanf(wNum, "%" SCNu32, &wRes);
    assert(s == 1);

    fprintf (stdout, "%8" PRIu32 " ==> %d\n", wRes, wRc);

    if (wRc)
    {
        if (mCur[wFoundC].lig != wFoundL)
        {
            mCur[wFoundC].lig = wFoundL;
            mCur[wFoundC].num1 = wRes;
            wRes = 0;
        }
        else
        {
            wRes *= mCur[wFoundC].num1;

            memset(&mCur[wFoundC], 0, sizeof(mCur[wFoundC]));
        }
    }
    else
    {
        wRes = 0;
    }

    return wRes; 
}


uint32_t doCompute(void)
{
    uint32_t wRes = 0;
    bool wF = false;
    size_t wStart, wEnd;


    for (size_t l = 0; l < mSize; l++)
    {
        for (size_t c = 0; c < mSize; c++)
        {
            if (isdigit(mGrid[l][c]))
            {
                if (wF)
                {
                    wEnd = c;
                }
                else
                {
                    wStart = c;
                    wEnd = c;
                    wF = true;
                }
            }
            else if (wF)
            {
                wRes += doCheck(l, wStart, wEnd);
                wStart = 0;
                wEnd = 0;

                wF = false;
            }
        }
        if (wF)
        {
            wRes += doCheck(l, wStart, wEnd);
            wStart = 0;
            wEnd = 0;

            wF = false;
        }
    }
    
    return wRes;
}

uint32_t doProcessFile(FILE *input)
{
    char line[256];
    size_t wIdx = 0;
    char * l = fgets(line, sizeof(line), input);

    while (!feof(input))
    {
        char *wEOL = strchr(l, '\n');
        assert(wEOL != NULL);

        if (mSize == 0)
        {
            mSize = wEOL - l;
        }
        memcpy(mGrid[wIdx], line, mSize);

        wIdx++;

        l = fgets(line, sizeof(line), input);
    }


    uint32_t wStage = doCompute();

    fprintf(stdout, "Total Share item : %" PRIu32 "\n", wStage);

    return wStage;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fclose(input);

    }

    return wres;
}
