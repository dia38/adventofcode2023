#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>

uint8_t mCards[10] = {255};

uint32_t mWinCard[255] = {0};

uint32_t doProcessFile(FILE *input)
{
    uint32_t    wStage1 = 0;

    char wLine[256] = { 0 };

    char *wL = fgets(wLine, sizeof(wLine), input);

    while (!feof(input))
    {
        uint32_t wScore = 0;
        uint32_t wCard;
        size_t wIdx = 0;
        char *wCurParser;
        char *wCurList;

        char *wToken = strtok_r(wLine, ":", &wCurParser);
        assert(wToken != NULL);

        int wRc = sscanf(wToken, "Card %" SCNu32, &wCard);
        assert(wRc == 1);
        mWinCard[wCard] += 1;

        wToken = strtok_r(NULL, "|", &wCurParser);
        assert(wToken != NULL);

        wToken = strtok_r(wToken, " ", &wCurList);
        while (wToken != NULL)
        {
            assert(wIdx  < sizeof(mCards));

            wRc = sscanf(wToken, "%" SCNu8, &mCards[wIdx]);
            assert(wRc == 1);

            wIdx++;
            wToken = strtok_r(NULL, " ", &wCurList);
        }

        wToken = strtok_r(NULL, "\n", &wCurParser);

        wIdx = wCard + 1;
        wToken = strtok_r(wToken, " ", &wCurList);
        while (wToken != NULL)
        {
            uint8_t wVal;
            wRc = sscanf(wToken, "%" SCNu8, &wVal);
            assert(wRc == 1);

            wRc = (memchr(mCards, wVal, sizeof(mCards)) != NULL);

            if (wRc)
            {
                mWinCard[wIdx] += mWinCard[wCard];

                wIdx++;
            }

            wToken = strtok_r(NULL, " ", &wCurList);
        }

        fprintf(stdout, "Score Card %" PRIu32 "(%" PRIu32 ") : %" PRIu32 "\n", wCard, wIdx, mWinCard[wCard]);

        wStage1 += mWinCard[wCard];

        wL = fgets(wLine, sizeof(wLine), input);
    }

    fprintf(stdout, "Total Share item : %" PRIu32 "\n", wStage1);

    return wStage1;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fclose(input);

    }

    return wres;
}
