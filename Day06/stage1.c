#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>

#define NB_STACK   9

typedef struct
{
    uint32_t    Time;
    uint32_t    Distance;
} ts_race;

static size_t mCount;
static ts_race mRace[NB_STACK];

int32_t doCompute(ts_race *parRace)
{
    int32_t wRes = 0;
    uint32_t wIdx = 0;

    for (wIdx = 0 ; (wIdx < (parRace->Time / 2)) && (wRes <= parRace->Distance) ; wIdx++)
    {
        wRes = (parRace->Time - wIdx) * wIdx;
    }
    wIdx --;

    wRes = (parRace->Time + 1) - ( 2 * wIdx );

    fprintf(stdout, "Time:%" PRIu32 ", Dist:%" PRIu32 ", Idx:%" PRIu32 ", Nb:%" PRId32 "\n", 
        parRace->Time, parRace->Distance, wIdx, wRes
    );
    
    return wRes;
}

uint32_t doProcessFile(FILE *input)
{
    uint32_t    wStage      = 1;
    size_t      wIdx         = 0;
    char        line[256];

    char *wL = fgets(line, sizeof(line), input);

    while (!feof(input))
    {
        char *wCur;
        char *wToken = strtok_r(wL, ":", &wCur);

        if (strcmp(wToken, "Time") == 0)
        {
            wToken = strtok_r(NULL, " ", &wCur);
            wIdx = 0;

            while (wToken != NULL)
            {
                int s = sscanf(wToken, "%" SCNu32, &mRace[wIdx].Time);
                assert(s == 1);

                wIdx++;
                wToken = strtok_r(NULL, " ", &wCur);
            }

            mCount = wIdx;
        }
        else if (strcmp(wToken, "Distance") == 0)
        {
            wToken = strtok_r(NULL, " ", &wCur);
            wIdx = 0;

            while (wToken != NULL)
            {
                int s = sscanf(wToken, "%" SCNu32, &mRace[wIdx].Distance);
                assert(s == 1);

                wIdx++;
                wToken = strtok_r(NULL, " ", &wCur);
            }
        }

        wL = fgets(line, sizeof(line), input);
    }

    for (wIdx = 0 ; wIdx < mCount ; wIdx++)
    {
        wStage *= doCompute(&mRace[wIdx]);
    }

    return wStage;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fprintf(stdout, "found marqueur at : %" PRIu32 "\n", total);
        fclose(input);

    }

    return wres;
}
