#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>

typedef struct
{
    uint32_t    Time;
    uint64_t    Distance;
} ts_race;

static ts_race mRace;

int32_t doCompute(ts_race *parRace)
{
    uint64_t wDist = 0;
    int32_t wRes = 0;
    uint32_t wIdx = 0;

    for (wIdx = 0 ; (wIdx < (parRace->Time / 2)) && (wDist <= parRace->Distance) ; wIdx++)
    {
        wDist = (parRace->Time - wIdx);
        wDist *= wIdx;

        if ((wDist > parRace->Distance) && (wRes == 0))
        {
            wRes = (parRace->Time + 1) - ( 2 * wIdx );
        }
    }
    wIdx --;


    fprintf(stdout, "Time:%" PRIu32 ", Dist:%" PRIu64 ", Idx:%" PRIu32 ", Nb:%" PRId32 "\n", 
        parRace->Time, parRace->Distance, wIdx, wRes
    );
    
    return wRes;
}

uint32_t doProcessFile(FILE *input)
{
    uint32_t    wStage      = 1;
    char        line[256];
    uint32_t    wTmp;

    char *wL = fgets(line, sizeof(line), input);

    while (!feof(input))
    {
        char *wCur;
        char *wToken = strtok_r(wL, ":", &wCur);

        if (strcmp(wToken, "Time") == 0)
        {
            wToken = strtok_r(NULL, " ", &wCur);
            mRace.Time = 0;

            while (wToken != NULL)
            {
                int s = sscanf(wToken, "%" SCNu32, &wTmp);
                assert(s == 1);

                if (wTmp < 10)
                {
                    mRace.Time *= 10;
                    mRace.Time += wTmp;
                }
                else if (wTmp < 100)
                {
                    mRace.Time *= 100;
                    mRace.Time += wTmp;
                }
                else if (wTmp < 1000)
                {
                    mRace.Time *= 1000;
                    mRace.Time += wTmp;
                }
                else if (wTmp < 10000)
                {
                    mRace.Time *= 10000;
                    mRace.Time += wTmp;
                }
                else
                {
                    assert(wTmp < 1000);
                }

                wToken = strtok_r(NULL, " ", &wCur);
            }
        }
        else if (strcmp(wToken, "Distance") == 0)
        {
            wToken = strtok_r(NULL, " ", &wCur);
            mRace.Distance = 0;

            while (wToken != NULL)
            {
                int s = sscanf(wToken, "%" SCNu32, &wTmp);
                assert(s == 1);

                if (wTmp < 10)
                {
                    mRace.Distance *= 10;
                    mRace.Distance += wTmp;
                }
                else if (wTmp < 100)
                {
                    mRace.Distance *= 100;
                    mRace.Distance += wTmp;
                }
                else if (wTmp < 1000)
                {
                    mRace.Distance *= 1000;
                    mRace.Distance += wTmp;
                }
                else if (wTmp < 10000)
                {
                    mRace.Distance *= 10000;
                    mRace.Distance += wTmp;
                }
                else
                {
                    assert(wTmp < 1000);
                }

                fprintf(stdout, "%" PRIu64 "\n", mRace.Distance);
                wToken = strtok_r(NULL, " ", &wCur);
            }
        }

        wL = fgets(line, sizeof(line), input);
    }

    wStage = doCompute(&mRace);

    return wStage;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fprintf(stdout, "found marqueur at : %" PRIu32 "\n", total);
        fclose(input);

    }

    return wres;
}
