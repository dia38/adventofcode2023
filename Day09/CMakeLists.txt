cmake_minimum_required(VERSION 3.5.0)
project(AventOfCode2023 VERSION 0.1.0 LANGUAGES C)

include(CTest)
enable_testing()

add_executable(D09_stage1 stage1.c)
add_executable(D09_stage2 stage2.c)
