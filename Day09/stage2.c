#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>

static int32_t mSeq[512][512];
static size_t mSeqSize[512];

static bool Compute(
    int32_t parSeq[], size_t parSize,
    int32_t parOut[], size_t *parOutSize    
)
{
    size_t  wRes    = 0;
    bool    wRc     = true;
    size_t  wIdx    = 0;

    assert(parSeq != NULL);
    assert(parOut != NULL);

    for (wIdx = 1, wRes = 0; wIdx < parSize ; wIdx++)
    {
        parOut[wRes] = parSeq[wIdx] - parSeq[wIdx - 1];
        wRc &= (parOut[wRes] == 0);
        wRes++;
    }

    if (parOutSize != NULL)
    {
        *parOutSize = wRes;
    }

    return wRc;
}

static int32_t doProcess(void)
{
    size_t      wIdx = 0;
    int32_t     wRes = 0;
    bool        wRc  = false;

    for (wIdx = 0; (wIdx < 512) && !wRc; wIdx++)
    {
        wRc = Compute(mSeq[wIdx] + 1, mSeqSize[wIdx], mSeq[wIdx + 1] + 1, &mSeqSize[wIdx + 1]);
    }

    mSeq[wIdx][0] = 0;

    for ( ; wIdx > 0 ; wIdx--)
    {
        wRes =  mSeq[wIdx - 1][1] - mSeq[wIdx][0];
        mSeq[wIdx - 1][0] = wRes;
        mSeqSize[wIdx - 1]++;
    }

    return wRes;
}


static int32_t doProcessFile(FILE *input)
{
    int32_t    wStage = 0;
    char        wLine[512];
    char *      wL;
    char *      wParser;

    wL = fgets(wLine, sizeof(wLine), input);
    while (wL != NULL)
    {
        wL = strtok_r(wL, " ", &wParser);
        assert(wL != NULL);
        mSeqSize[0] = 0;

        while (wL != NULL)
        {
            int s = sscanf(wL, "%" SCNu32, &mSeq[0][mSeqSize[0] + 1]);
            assert(s == 1);

            mSeqSize[0]++;
            wL = strtok_r(NULL, " ", &wParser);
        }

        wStage += doProcess();

        wL = fgets(wLine, sizeof(wLine), input);
    }

    return wStage;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        int32_t total = 0;

        total = doProcessFile(input);

        fprintf(stdout, "Total visible : %" PRId32 "\n", total);
        fclose(input);

    }

    return wres;
}
